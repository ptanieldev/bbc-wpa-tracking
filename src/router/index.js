import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/files/Home";
import Actives from "../components/files/Actives";
import Archived from "../components/files/Archived";
import News from "../components/files/News";
import Rates from "../components/files/Rates";


import Authentication from "../components/files/admin/Authentication";
//import dataStore from "../store/index";

Vue.use(VueRouter);
export default new VueRouter({
    mode: "history",
    routes: [


        { path: "/Login", component: Authentication },
        { path: "/Actives", component: Actives },
        { path: "/Archived", component: Archived },
        { path: "/News", component: News },
        { path: "/Rates", component: Rates },
        
        {
            path: "/Home", component: Home,

        },


        { path: "*", redirect: "/login" }
    ]
})