const CACHE_NAME='V1_cache_BBC_APP';
const urlsToCache=[
    './',
    './js/app.28933ae9.js',
    './js/app.28933ae9.js.map',
    './js/chunk-vendors.1a4aa2bc.js',
    './js/chunk-vendors.1a4aa2bc.js.map',
    
    
    './css/app.c3cd27d5.css',
    
    './css/chunk-vendors.f2308f31.css',
    './fonts/fontawesome-webfont.674f50d2.eot',
    './fonts/fontawesome-webfont.af7ae505.woff2',
    './fonts/fontawesome-webfont.b06871f2.ttf',
    './fonts/fontawesome-webfont.fee66e71.woff',
    
    
    
    
    
    
    
    './img/fontawesome-webfont.912ec66d.svg',
    './icons/Documents-icon.png',
    
     './img/logo.png',
    './img/details.jpeg',
    
    './index.html',
    './manifest.json',
    './workbox-344c916c.js',
    './workbox-344c916c.js.map',
    './sw.js',
    './icons/Documents-icon 64x64.png',
    './icons/Documents-icon 72x72.png',
    './icons/Documents-icon 96x96.png',
    './icons/Documents-icon 128x128.png',
    './icons/Documents-icon 512x512.png',



]

this.addEventListener('install', function(event) {
    console.log('installing....');
    event.waitUntil(
        caches.open('v1').then(function(cache) {
            return cache.addAll([
                'index.html', 
                './',
'./css/app.28933ae9.js',
'./css',
'./js',
'./img',
'./fonts',


'./css/chunk-vendors.1a4aa2bc.js',
'./fonts/fontawesome-webfont.674f50d2.eot',
'./fonts/fontawesome-webfont.af7ae505.woff2',
'./fonts/fontawesome-webfont.b06871f2.ttf',
'./fonts/fontawesome-webfont.fee66e71.woff',

'./icons',

'./js/app.28933ae9.js.map',

'./js/chunk-vendors.1a4aa2bc.js.map',

'./img/fontawesome-webfont.912ec66d.svg',
'./Documents-icon.png',
'./home-icon.png',
'./arrowu.png',
'./arrowd.png',
'./sales.png',
'./logo.png',
'./s.png',

'./manifest.json',
'./workbox-344c916c.js',
'./workbox-344c916c.js.map',
'./sw.js',
'./icons/Documents-icon 64x64.png',
'./icons/Documents-icon 72x72.png',
'./icons/Documents-icon 96x96.png',
'./icons/Documents-icon 128x128.png',
'./icons/Documents-icon 512x512.png',
 ]);
            })
        );
    });
    this.addEventListener('fetch', function(event) {
        event.respondWith(
            caches.open('v1').then(function(cache) {
                return cache.match(event.request).then(function(response) {
                    return response || fetch(event.request).then(function(response) {
                        cache.put(event.request, response.clone());
                        return response;
                    });
                });
            })
        );
    });
    this.addEventListener('activate', function activator(event) {
        console.log('activate!');
        event.waitUntil(
            caches.keys().then(function(keys) {
                return Promise.all(keys
                    .filter(function(key) {
                        return key.indexOf('v1') !== 0;
                    })
                    .map(function(key) {
                        return caches.delete(key);
                    })
                );
            })
        );
    });
