import Vue from 'vue';
import App from  './App.vue';


Vue.config.productionTip = false
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css"
import Vuelidate from "vuelidate";

import router from "./router";

import store from "./store/index";

Vue.use(Vuelidate);

new Vue({   
  render: h => h(App),
  provide: function () {
    return {
    eventBus: new Vue()
    }
    },
  router,
  store,
 

  
}).$mount("#app");
