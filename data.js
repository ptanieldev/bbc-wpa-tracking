const { date } = require("faker");

var data = [{
    id: 1585, serviceType: "DDP", consignee: "Consignee No1",
    origin: "New York", destination: "Nicaragua", reference: "Ref #100", archived: 0,
    status: "transit", location: "Miami",date: "07/07/2020", container: "Container A",
    bbcServices: "BBC service A",bl: "BL 1",etd:"05/07/2020", eta:"10/07/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Nicaragua", pafOrigin: "Bluefields"


},
{
    id: 2489 , serviceType: "DPU", consignee: "Consignee No2",
    origin: "Shenzhen", destination: "Guatemala", reference: "Ref #1200", archived: 0,
    status: "delivered", location: "Florida",date: "09/05/2020", container:"Container B",
    bbcServices: "BBC service B",bl: "BL 2",etd:"05/03/2020", eta:"09/05/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Guatemala", pafOrigin: "Puerto Quetzal"
},
{
    id: 3584, serviceType: "DAP", consignee: "Consignee No3",
    origin: "Madrid", destination: "Costa Rica", reference: "Ref #1002", archived: 1,
    status: "delivered", location: "Costa Rica",date: "12/11/2020",container:"Container C",
    bbcServices: "BBC service C",bl: "BL 3",etd:"05/11/2020", eta:"19/11/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Costa Rica", pafOrigin: "Puerto Limon"
},
{
    id: 4874, serviceType: "FCA", consignee: "Consignee No4",
    origin: "Paris", destination: "Costa Rica", reference: "Ref #12", archived: 1,
    status: "transit", location: "Miami",date: "02/14/2020", container: "Container D",
    bbcServices: "BBC service D",bl: "BL 4",etd:"01/02/2020", eta:"09/03/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Costa Rica", pafOrigin: "Puerto Limon"
},
{
    id: 5854, serviceType: "CPT", consignee: "Consignee No6",
    origin: "London", destination: "Atlanta", reference: "Ref #440", archived: 1,
    status: "customs", location: "Miami",date: "07/08/2020", container: "Container E",
    bbcServices: "BBC service E",bl: "BL 5",etd:"05/03/2020", eta:"09/08/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Atlanta", pafOrigin: "Port of Atlanta"
},
{
    id: 6154, serviceType: "FOB", consignee: "Consignee No7",
    origin: "Seoul", destination: "Los Angeles", reference: "Ref #455", archived: 0,
    status: "transit", location: "Miami",date: "03/01/2020", container: "Container F",
    bbcServices: "BBC service F",bl: "BL 6",etd:"02/01/2020", eta:"09/01/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Los Angeles", pafOrigin: "Americas Port"
},
{
    id: 7045, serviceType: "EXW", consignee: "Consignee No8",
    origin: "Hong Kong", destination: "El Salvador", reference: "Ref #990", archived: 1,
    status: "started", location: "Miami",date:"09/05/2020", container: "Container G",
    bbcServices: "BBC service G",bl: "BL 7",etd:"05/05/2020", eta:"09/06/2020",
    shipper: "BBC Logistica S.A",pafDestination: "El Salvador", pafOrigin: "Puerto La Libertad"
},
{
    id: 8854, serviceType: "FCA", consignee: "Consignee No9",
    origin: "Chicago", destination: "Honduras", reference: "Ref #800", archived: 0,
    status: "started", location: "Miami",date: "04/05/2020", container: "Container H",
    bbcServices: "BBC service H",bl: "BL 8",etd:"05/03/2020", eta:"09/05/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Honduras", pafOrigin: "Puerto Cortez"
},
{
    id: 9524, serviceType: "DDP", consignee: "Chess",
    origin: "Toronto", destination: "Panama", reference: "Ref #2300", archived: 1,
    status: "transit", location: "Miami",date: "12/10/2020", container:"Container I",
    bbcServices: "BBC service I",bl: "BL 9",etd:"05/10/2020", eta:"19/10/2020",
    shipper: "BBC Logistica S.A",pafDestination: "Panama", pafOrigin: "Puerto Almirante"

}]

var contacts = [
    {
        id: 1585, sales: "Bob Boone", operative: "Wally Joyner",
        customs: "Johnny Ray", client: "Dick Schofield"
    },

    {
        id: 2489, sales: "Doug DeCinces", operative: "Jack Howell",
        customs: "Gary Pettis", client: "Devon White"
    },

    {
        id: 3584, sales: "Brian Downing", operative: "Kirk McCaskill",
        customs: "Chuck Finley", client: "Willie Fraser"
    },

    {
        id: 4874, sales: "Gary Carter", operative: "Keith Hernandez",
        customs: "Wally Backman", client: "Rafael Santana"
    },

    {
        id: 5854, sales: "Howard Johnson", operative: "Kevin McReynolds",
        customs: "Lenny Dykstra", client: "Darryl Strawberry"
    },

    {
        id: 6154, sales: "Terry Kennedy", operative: "Eddie Murray",
        customs: "Billy Ripken", client: "Carl Ripken Jr"
    },

    {
        id: 7045, sales: "Ray Knight", operative: "Larry Sheets",
        customs: "Fred Lynn", client: "Lee Lacy"
    },

    {
        id: 8854, sales: "Mike Young", operative: "Scott McGregor",
        customs: "Mike Flanagan", client: "Luis De Leon"
    },

    {
        id: 9524, sales: "Jose Mesa", operative: "Jeff Ballard",
        customs: "Mike Griffin", client: "Don Aese"
    }








]

function filter(flag) {
    let status = [];

    for (let i = 0; i < data.length; i++) {
        if (data[i].archived === flag) {

            status.push(data[i]);
        }
    }
    return status;
}
module.exports = function () {
    return {
        products: data,
        archived: filter(1),
        actives: filter(0),
        contacts: contacts,
    }
}