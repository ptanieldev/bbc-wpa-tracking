import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import AuthModule from "./auth";
import Noti from "./notifications"



Vue.use(Vuex);
//const baseUrl=  "http://localhost:3500";
const baseUrl = "http://165.22.5.93:5001";
const activesUrl = `${baseUrl}/actives`;
const archivedUrl = `${baseUrl}/archived`;

const contactsUrl = `${baseUrl}/contacts`;



export default new Vuex.Store({
    strict: false,
    modules: { auth: AuthModule, Note: Noti },
    state: {
        actives: [],
        archived: [],
        contacts: [],
        categoriesData: [],

        productsTotal: 0,
        currentPage: 1,
        pageSize: 4,
        currentCategory: 0

    },

    getters: {




        processedActives: (state) => {
            let index = (state.currentPage - 1) * state.pageSize;

            return state.actives.slice(index,
                index + state.pageSize);
        },
        pageCount: (state) =>
            Math.ceil(state.actives.length / state.pageSize),

        processedArchived: (state) => {
            let index = (state.currentPage - 1) * state.pageSize;

            return state.archived.slice(index,
                index + state.pageSize);
        },

        pageCount2: (state) =>
            Math.ceil(state.archived.length / state.pageSize)
    },
    mutations: {
        setCurrentPage(state, page) {
            state.currentPage = page;
        },
        setPageSize(state, size) {
            state.pageSize = size;
            state.currentPage = 1;
        },
        setData(state, data) {
            state.actives = data.pdata;
            state.archived = data.archivedData;
            state.productsTotal = data.pdata.length;

        },

        setDataContacts(state, contacts) {
            state.contacts = contacts.codata;

        }
    },

    actions: {
        async getData(context) {
            let pdata = (await Axios.get(activesUrl)).data;
            let archivedData = (await Axios.get(archivedUrl)).data;
            let codata = (await Axios.get(contactsUrl)).data;

            context.commit("setData", { pdata, archivedData });
            context.commit("setDataContacts", { codata });
        },


    }
})