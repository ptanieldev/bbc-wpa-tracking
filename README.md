#WPA for BBC Tracking System
The goal of the projec is the development of  a Web Progressive App (WPA) for BBC tracking system

##General information
Project was developed using VUE.js framework, files of the application are located into the following directory structure:

/                       Root directory with the general files of the application
/components             Contains the Vue components
/components/store       Files for the data store
/components/router      Files for the URL routing system
/public                 Contains html, images, service worker, manifest, and    icons files
/dist                   Compiled files for production purposes



VUE components:
App-->  Main entrance for the application, file name= src/App.vue

Authentication--> Component for register on to the application, using          RESTful  service and vuelidate package, file on=src/componets/files/admin/Authentication.vue

ValidationError--> Validate correct login process,file= src/components/files/ValidationError

Burger--> Component that provides the Burguer menu style into the application,
file= src/components/files/Burger.vue

SideBar--> Sliding bar menu used to navigate into the application file=src/componemts/files/SideBar.vue

Home--> Home page of the application, file= src/components/files/Home.vue

PageControls--> Provide pagination functionality to the listing components
file= src/components/files/PageControls.vue

Actives--> Shows the actives SO´s, including detail information, contacts and tracking information, file=src/components/files/Actives.vue

Archived--> Show information about de stored SO´s fie=src/componets/Archived.vue

News--> Placeholder for news, file= src/components/files/News.vue

Rates--> Placeholder for rates, file= src/components/files/Rates.vue

Menu--> Provides the menu for the application, file= src/components/files/Menu.vue



Java Script files

    /src/main.js--> Create Vue object
    /src/router/index.js--> This file is used to configure the URL routing system
    /src/store/index.js --> Used to configure de data store for the application
    /src/store/auth.js--> Used in the data store to access the autenthication process
    /AuthMiddleware.js--> Used for authentication on the RESTful server
    /data.js--> Provides the data in the RESTful server
    public/sw.js--> Service Worker file for the WPA
    
Other files

/public/index.html--> Contains the configuration for access de application through the web server 
manifest.json--> Manifest file for the WPA
/img-->images for the application
/icons--> icons that identify the BBC WPA on the desktop once installed


##Requirements

To run the development environment install the following packages:

vue tools package-->  npm install --global @vue/cli
bootstrap, CSS framework--> npm install bootstrap@4.0.0
vue-router, URL routing system--> npm install vue-router@3.0.1
axios package HTTP request tool-->npm install axios@0.18.0
vuex,data store package--> npm install vuex@3.0.1
veulidate,data validation package--> npm install vuelidate@0.7.4
font-awesome, library of icons--> npm install font-awesome@4.7.0
json-server RESTful web service--> npm install --save-dev json-server@0.12.1
jsonwebtoke, authorization tokens package-->npm install --save-dev jsonwebtoken@8.1.1

Packages installed with the --save-dev argument means: for development purposes only

##Parts of the application

###General configuration files:

main.js file
Located under the /src  directory, used to create the Vue object and configures the additional packages.

i.e creation of vue object: new Vue(render: h => h(App))

Integration of the packages into the system:
import "bootstrap/dist/css/bootstrap.min.css";

App.vue file
This is the root component, which is  used as the point at which 
custom application content begins

Example:
export default {
 name: 'app',

package.json file
Used for configuration of the packages installed, into the BBC App is used to configure the Json RESTful 
web service

###URL Routing system

To manage the URL routing into the application, the configuration is applied through the file src/router/index.js. Into this file Vue components links must be configured to be accesed, i.e:

import Home from "../components/files/Home";
{ path: "/Home", component: Home }

###Data store system

To access the data on the development enviroment, configuration file src/store/index.js is used provide the means to access the data located into the RESTful web server.
packages used into data store:
import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";


URLs to access the RESTful web server:

baseUrl = "http://localhost:3500";
activesUrl = `${baseUrl}/actives`;
archivedUrl = `${baseUrl}/archived`;
contactsUrl = `${baseUrl}/contacts`;


The src/store/index.js file create a Vuex.Store object that consist of the following  parts:

state properties-->actives (data of actives SOs), archived (data of archived SOs)
contancts (data of contacts); productsTotal,currentPage and pageSize used for pagination facility

getters:
Used to access and process the data, the following getters was configured:
processedActives: to access actives SOs into the data store
processedArchived: to acces archived SOs into the data store
pageCount: for pagination functionality 

mutations:
Needed to modify the data store information, the following mutations was created:

setCurrentPage: To set the current page when use the pagination functionality
setPageSize: Define the number of SOs diplayed per page
setData: Insert SOs the data obtained from the RESTful web server into the data store
setDataContacts: Insert contact information obtained from the RESTful web server into the data store

Actions: Used for async connection to obtain the data from the RESTful web server
the file contains the following actions:

getData: For connection to the RESTful web server to obtain the SOs and Contatcs information

###Authentication process
To authenticate the application under the development environment the following parts was used:

/authMiddleware.js file, configure the information needed at the RESTful web server side.
/src/store/auth.js, used to provide access for the authorization process on the data store side

Authentication.vue, component of Vue that provide the login access to the BBC App
ValidationError.vue, provide login error management

###application Components

Navigation and menu tools
Burger.vue, this component provide the menu type burger to the application, this is imported by the Menu.vue component to provide this facility to the app

SideBar.vue, this component provides the sliding bar menu to the application in order to access the parts of the app, this component is imported by the Main.vue component

Menu.vue, Provides the menu options for the application:
----Home
----Actives
----Archived
----News
----Rates

This component use the URL routing system and the Burger and SideBar components to display the app menu

pageControls, provides pagination funtionality to the Actives and Archived components, set the current page, numbering pages, page shift options




In order to access and display the information of the BBC tracking system the following components are part of the application:
Home
Actives
Archived
News 
Rates

Home component: this is the home page of the app, is intended to place important information about the BBC tracking system

Actives: Main panel for the actives SOs view

Archive: Main panel for the archived SOs view

ActivesList component: Used to display SOs information, including details of each one, contact information and trackig logs, 

ArchivedList: Shows information about stored SOs

News: Placeholder for news

Rates: Placeholder for rates information

##Run the development environment
To run the BBC app under the development environment, two services must be running:

npm run serve
npm run json

Open the link localhost:8080/login
username: admin
password: secret

##Build the App

To build the files for production, the following process must be followe

npm run build

to test the production files, it can run the following command

serve -s dist 

open a browser with the following link: localhost:5000/login


EOF
























